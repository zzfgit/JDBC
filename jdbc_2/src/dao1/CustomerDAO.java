package dao1;

import bean.Customer;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

/*
* 用于规范针对于customers表的常用操作接口
* */
public interface CustomerDAO {

    /*
    * 将customer对象添加到数据库中
    * */
    void insert(Connection connection, Customer customer);

    /*
    * 针对指定的ID,删除表中的一条纪律
    * */
    void deleteById(Connection connection, int id);

    /*
    * 针对内存中的customer修改表中的记录
    * */
    void update(Connection connection, Customer customer);

    /*
    * 针对指定的ID查询得到对应的Customer对象
    * */
    Customer getCustomerByID(Connection connection, int id);

    /*
    * 查询表中所有记录构成的集合
    * */
    List<Customer> getAll(Connection connection);

    /*
    * 返回数据表中数据的条目数
    * */
    long getCount(Connection connection);

    /*
    * 返回数据表中最大的生日
    * */
    Date getMaxBirth(Connection connection);
}
