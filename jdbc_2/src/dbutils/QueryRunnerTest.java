package dbutils;

import bean.Customer;
import connection.util.JDBCUtilsDruid;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.*;
import org.junit.Test;

import java.sql.Connection;
import java.util.List;
import java.util.Map;


/*
* commons-dbutils 时Apache组织提供的一个开源的JDBC工具类库,封装了针对于数据库的增删改查操作
*
* */
public class QueryRunnerTest {

    /*
    * 测试插入数据操作
    * */
    @Test
    public void testInsert() {
        Connection connection = null;
        try {
            QueryRunner runner = new QueryRunner();
            connection = JDBCUtilsDruid.getConnection1();
            String sql = "insert into customers(name,email,birth) values(?,?,?)";
            int insertCount = runner.update(connection, sql, "蔡徐坤1", "caixukun@126.com", "1997-9-9");
            System.out.println(insertCount);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtilsDruid.closeResouce(connection,null);
        }
    }

    /*
    * 测试查询操作
    * BeanHandler:是ResultSetHandler接口的实现类,用于封装表中的一条记录
    * */
    @Test
    public void testQuery(){
        Connection connection = null;
        try {
            QueryRunner queryrunner = new QueryRunner();
            connection = JDBCUtilsDruid.getConnection1();
            String sql = "select id,name,email,birth from customers where id = ?";

            //提供一个结果集处理器,这个处理器可以是对象,可以是map,也可以是ArrayList
            BeanHandler<Customer> customerBeanHandler = new BeanHandler<Customer>(Customer.class);
            //获取到查询结果对象
            Customer customer = queryrunner.query(connection, sql, customerBeanHandler, 23);
            System.out.println(customer);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtilsDruid.closeResouce(connection,null);
        }
    }
    /*
     * 测试查询操作2
     * BeanListHandler:是ResultSetHandler接口的实现类,用于封装表中的一组记录
     * */
    @Test
    public void testQuery2(){
        Connection connection = null;
        try {
            QueryRunner queryrunner = new QueryRunner();
            connection = JDBCUtilsDruid.getConnection1();
            String sql = "select id,name,email,birth from customers where id < ?";

            //提供一个结果集处理器,这个处理器可以是对象,可以是map,也可以是ArrayList
            BeanListHandler<Customer> customerBeanHandler = new BeanListHandler<Customer>(Customer.class);
            //获取到查询结果对象集合
            List<Customer> customers = queryrunner.query(connection, sql, customerBeanHandler, 23);
            for (Customer customer : customers) {
                System.out.println(customer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtilsDruid.closeResouce(connection,null);
        }
    }

    /*
     * 测试查询操作3
     * MapHandler:是ResultSetHandler接口的实现类,对应表中的一条记录,以Map的形式返回
     * */
    @Test
    public void testQuery3(){
        Connection connection = null;
        try {
            QueryRunner queryrunner = new QueryRunner();
            connection = JDBCUtilsDruid.getConnection1();
            String sql = "select id,name,email,birth from customers where id = ?";

            //提供一个结果集处理器,这个处理器可以是对象,可以是map,也可以是ArrayList
            MapHandler mapHandler = new MapHandler();
            //获取到查询结果对象
            Map map = queryrunner.query(connection, sql, mapHandler, 23);
            System.out.println(map);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtilsDruid.closeResouce(connection,null);
        }
    }

    /*
     * 测试查询操作4
     * MapListHandler:是ResultSetHandler接口的实现类,对应表中的一组记录,用数组包map的形式返回一组数据
     * */
    @Test
    public void testQuery4(){
        Connection connection = null;
        try {
            QueryRunner queryrunner = new QueryRunner();
            connection = JDBCUtilsDruid.getConnection1();
            String sql = "select id,name,email,birth from customers where id < ?";

            //提供一个结果集处理器,这个处理器可以是对象,可以是map,也可以是ArrayList
            MapListHandler mapListHandler = new MapListHandler();
            //获取到查询结果对象集合
            List<Map<String, Object>> mapList = queryrunner.query(connection, sql, mapListHandler, 23);
            mapList.forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtilsDruid.closeResouce(connection,null);
        }
    }

    /*
     * 测试查询操作5:查询特殊值
     * ScalarHandler:是ResultSetHandler接口的实现类,用于查询特殊值,比如个数,最大,最小数等等
     * */
    @Test
    public void testQuery5(){
        Connection connection = null;
        try {
            QueryRunner queryrunner = new QueryRunner();
            connection = JDBCUtilsDruid.getConnection1();
            String sql = "select count(*) from customers";

            //提供一个结果集处理器,这个处理器可以是对象,可以是map,也可以是ArrayList
            ScalarHandler scalarHandler = new ScalarHandler();

            long count = (long)queryrunner.query(connection,sql,scalarHandler);
            System.out.println("customers表中的总数据条数是:" + count);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtilsDruid.closeResouce(connection,null);
        }
    }
}
