package connection;

import com.mchange.v2.c3p0.DataSources;
import org.junit.Test;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;


public class C3P0Test {
    //获取连接池中的连接的方式一:
    @Test
    public void testGetConnection() throws Exception {
        //获取c3p0数据库连接池
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass("com.mysql.cj.jdbc.Driver");
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test");
        dataSource.setUser("root");
        dataSource.setPassword("huhuhuhu");

        //设置相关的参数,对数据库连接池进行管理
        //设置初始时数据库连接池中的连接数
        dataSource.setInitialPoolSize(10);

        //获取一个数据库连接
        Connection connection = dataSource.getConnection();
        System.out.println(connection);

        //销毁连接池
//        DataSources.destroy(dataSource);
    }

    //方式二:
    //将配置信息写入到配置文件
    @Test
    public void testGetConnection2() throws SQLException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource("helloc3p0");
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
    }
}
