package connection.util;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JDBCUtilsDruid {

    /*
    * 使用c3p0获取数据库连接
    * */
    //连接池只需要一个即可
    private static ComboPooledDataSource dataSource = new ComboPooledDataSource("helloc3p0");
    //从连接池中获取一个连接
    public static Connection getConnection() throws Exception{
        Connection connection = dataSource.getConnection();
        System.out.println(connection);
        return connection;
    }


    //使用德鲁伊从连接池中获取一个连接
    static DruidDataSource druidDataSource;
    static {
        try {
            Properties pros = new Properties();
            InputStream resourceAsStream = ClassLoader.getSystemClassLoader().getResourceAsStream("druidJdbc.properties");
            pros.load(resourceAsStream);
            druidDataSource = (DruidDataSource) DruidDataSourceFactory.createDataSource(pros);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static Connection getConnection1() throws Exception{
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(connection);
        return connection;
    }

    /*
     * 关闭连接和statement的资源
     * */
    public static void closeResouce(Connection connection, Statement ps){
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (ps != null)
                ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
     * 关闭资源
     * */
    public static void closeResouce(Connection connection, Statement ps, ResultSet resultSet){
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (ps != null)
                ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null)
                resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
