package transaction;

import org.junit.Test;
import transaction.bean.User;
import util.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.*;

/*
 * 数据库事务:
 *   一组逻辑单元,使数据从一种状态变换到另一种状态
 *       >一组逻辑操作单元,一个或多个DML操作
 *
 * 事务的处理原则:
 *   事务中的所有操作作为一个工作单元来执行,如果其中某一个操作出现了故障,就要把已执行的操作回滚到执行前的状态
 *
 *注意:数据一旦提交(commit),就不可回滚
 *   DDL:会自动提交;
 *   DML:默认会自动提交;
 *       可以通过set autocommit = false 的方式取消自动提交
 *   关闭连接是:会自动触发提交操作.
 *
 * */
public class connectionTest {

    //*********************未考虑数据库事务的转账操作*********************
    /*
     * 针对于数据表user_table
     * AA用户给BB用户转账100
     * */
    @Test
    public void testUpdate() {
        String sql1 = "update user_table set balance = balance - 100 where user = ?";
        update(sql1, "AA");

        //模拟网络异常
        System.out.println(10 / 0);

        String sql2 = "update user_table set balance = balance + 100 where user = ?";
        update(sql2, "BB");

        System.out.println("转账成功");
    }

    //通用的增删改操作(对于增删改操作) --- version 1.0
    public int update(String sql, Object... args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            //1:获取数据库连接
            connection = JDBCUtils.getConnection();

            //2:预编译sql语句
            preparedStatement = connection.prepareStatement(sql);

            //3:填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1, args[i]);
            }

            //4:执行
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5:关闭
            JDBCUtils.closeResouce(connection, preparedStatement);
        }
        return 0;
    }


    @Test
    public void testUpdateWithTX() throws SQLException {

        Connection connection = null;

        try {
            connection = JDBCUtils.getConnection();

            //取消数据的自动提交功能
            connection.setAutoCommit(false);

            String sql1 = "update user_table set balance = balance - 100 where user = ?";
            update(connection, sql1, "AA");

            //模拟网络异常
            System.out.println(10 / 0);

            String sql2 = "update user_table set balance = balance + 100 where user = ?";
            update(connection, sql2, "BB");

            System.out.println("转账成功");

            //提交数据
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
            //回滚数据
            System.out.println("转账发生异常,回滚操作");
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {

            //在连接使用完成,关闭之前,把连接的自动提交功能设置会原来的值(在使用数据库连接池的时候这么做)
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            JDBCUtils.closeResouce(connection, null);
        }
    }

    @Test
    public void testTransactionSelect() throws Exception {
        Connection connection = JDBCUtils.getConnection();
        //获取当前连接的隔离级别
        System.out.println(connection.getTransactionIsolation());

        //设置隔离级别
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

        //取消自动提交
        connection.setAutoCommit(false);

        String sql = "select user,password,balance from user_table where user = ?";
        User cc = getInstance(connection, User.class, sql, "CC");
        System.out.println(cc);
    }

    @Test
    public void testTransactionUpdate() throws Exception {
        Connection connection = JDBCUtils.getConnection();

        //取消自动提交
        connection.setAutoCommit(false);

        String sql = "update user_table set balance = ? where user = ?";
        update(connection, sql, 5000, "CC");
        Thread.sleep(1000);
        System.out.println("修改结束");
        connection.commit();
        connection.close();
    }

    //*********************考虑数据库事务的转账操作*********************
    //通用的增删改操作(对于增删改操作) --- version 2.0(考虑事务问题)
    public int update(Connection conn, String sql, Object... args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            //1:预编译sql语句
            preparedStatement = conn.prepareStatement(sql);

            //2:填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1, args[i]);
            }

            //3:执行
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5:关闭
            JDBCUtils.closeResouce(null, preparedStatement);
        }
        return 0;
    }

    //针对不同表的通用查询操作方法
    //此时返回表中的一条数据
    //2.0版本
    public <T> T getInstance(Connection conn, Class<T> clazz, String sql, Object... args) {
//        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            conn = JDBCUtils.getConnection();
            preparedStatement = conn.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1, args[i]);
            }

            //执行,获取结果集
            resultSet = preparedStatement.executeQuery();

            //获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //获取列数
            int columnCount = metaData.getColumnCount();
            if (resultSet.next()) {

                T t = clazz.newInstance();

                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过ResultSet
                    Object columnValue = resultSet.getObject(i + 1);
                    //获取列的别名
                    String columnLabel = metaData.getColumnLabel(i + 1);
                    //通过反射,将对象指定名columnName的属性赋值为指定的值columnValue
                    Field field = clazz.getDeclaredField(columnLabel);
                    field.setAccessible(true);
                    field.set(t, columnValue);
                }
                return t;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(null, preparedStatement, resultSet);
        }

        return null;
    }

}
