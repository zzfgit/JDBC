package dao.junit;

import bean.Customer;
import dao.CustomerDAOImpl;
import org.junit.Test;
import util.JDBCUtils;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

public class CustomerDAOImplTest {
    private CustomerDAOImpl dao = new CustomerDAOImpl();

    @Test
    public void testInsert() throws Exception {
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Customer customer = new Customer(1, "小李", "xiaoli.@126.com", new Date(12341234123L));
            dao.insert(connection,customer);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,null);
            System.out.println("添加成功");
        }
    }

    @Test
    public void testDeleteById(){
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            dao.deleteById(connection,12);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,null);
            System.out.println("删除成功");
        }
    }

    @Test
    public void testUpdateConnectCustomer(){
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();
            Customer customer = new Customer(18,"贝多芬","beiduofen@126.com",new Date(23423452345234L));
            dao.update(connection,customer);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,null);
            System.out.println("修改成功");
        }
    }

    @Test
    public void testGetCustomerById(){
        Connection connection = null;
        try {
            connection = JDBCUtils.getConnection();

            Customer customer = dao.getCustomerByID(connection, 19);

            System.out.println(customer);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,null);
            System.out.println("获取成功");
        }
    }

    @Test
    public void testGetAll(){
        Connection connection = null;

        try {
            connection = JDBCUtils.getConnection();
            List<Customer> all = dao.getAll(connection);
            for (Customer customer1 : all) {
                System.out.println(customer1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,null);
            System.out.println("获取成功");
        }
    }
}
