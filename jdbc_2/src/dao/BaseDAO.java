package dao;

import util.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/*
 DAO : data access object
* 封装了对于数据表的通用操作
*
* */
public abstract class BaseDAO {

    //通用的增删改操作(对于增删改操作) --- version 2.0(考虑事务问题)
    public int update(Connection conn, String sql, Object... args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            //1:预编译sql语句
            preparedStatement = conn.prepareStatement(sql);

            //2:填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1, args[i]);
            }

            //3:执行
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5:关闭
            JDBCUtils.closeResouce(null, preparedStatement);
        }
        return 0;
    }

    //针对不同表的通用查询操作方法,此时返回表中的一条数据
    //2.0版本
    public <T> T getInstance(Connection conn, Class<T> clazz, String sql, Object... args) {
//        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            conn = JDBCUtils.getConnection();
            preparedStatement = conn.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1, args[i]);
            }

            //执行,获取结果集
            resultSet = preparedStatement.executeQuery();

            //获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //获取列数
            int columnCount = metaData.getColumnCount();
            if (resultSet.next()) {

                T t = clazz.newInstance();

                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过ResultSet
                    Object columnValue = resultSet.getObject(i + 1);
                    //获取列的别名
                    String columnLabel = metaData.getColumnLabel(i + 1);
                    //通过反射,将对象指定名columnName的属性赋值为指定的值columnValue
                    Field field = clazz.getDeclaredField(columnLabel);
                    field.setAccessible(true);
                    field.set(t, columnValue);
                }
                return t;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(null, preparedStatement, resultSet);
        }

        return null;
    }

    //针对不同表的查询操作,返回一组数据
    //2.0版本
    public <T> List<T> getForList(Connection connection,Class<T> clazz, String sql, Object ...args){
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1,args[i]);
            }

            //执行,获取结果集
            resultSet = preparedStatement.executeQuery();

            //获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //获取列数
            int columnCount = metaData.getColumnCount();

            //创建集合对象
            ArrayList<T> list = new ArrayList<>();
            while (resultSet.next()) {

                T t = clazz.newInstance();

                //给t对象赋值的过程
                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过ResultSet
                    Object columnValue = resultSet.getObject(i + 1);
                    //获取列的别名
                    String columnLabel = metaData.getColumnLabel(i + 1);
                    //通过反射,将对象指定名columnName的属性赋值为指定的值columnValue
                    Field field = clazz.getDeclaredField(columnLabel);
                    field.setAccessible(true);
                    field.set(t,columnValue);
                }
                list.add(t);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(null,preparedStatement,resultSet);
        }

        return null;
    }

    //用户查询特殊值的通用方法
    public <E> E getValue(Connection connection,String sql,Object ...args){
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i+i,args[i]);
            }
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return (E)resultSet.getObject(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(null,preparedStatement,resultSet);
        }
        return null;
    }
}
