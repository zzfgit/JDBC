package blob;

import org.junit.Test;
import util.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/*
* 使用PreparedStatement实现批量操作数据的操作
*
* update,delete本身就具有批量操作的效果
* 此时的批量操作主要指的是批量插入.使用PreparedStatement实现更高效的批量插入
*
* 向goods表中插入20000条数据
* create table goods(
    id int primary key auto_increment,
    name varchar(25)
);
* */
public class InsertTest {

    //批量插入使用PreparedStatement
    @Test
    public void test1() {
        Connection connection = null;
        PreparedStatement ps = null;
        try {

            long start = System.currentTimeMillis();

            connection = JDBCUtils.getConnection();

            String sql = "insert into goods(name)values (?)";
            ps = connection.prepareStatement(sql);

            for (int i = 0; i < 20000; i++) {
                ps.setObject(1, "name_" + i);
                ps.execute();
            }

            long end = System.currentTimeMillis();

            System.out.println("花费的时间为:" + (end - start));//花费的时间为:10726毫秒
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection, ps);
        }
    }


    /*
     * 批量插入方式二:
     * 1:addBatch(),executeBatch(),clearBatch()
     * 2:mySql服务器默认是关闭批处理的,需要通过一个参数,让MySQL开启批处理的支持:?RewriteBatchedStatements=true 写在配置文件的URL后边(但是MySQL5.1.37之后的驱动版本都默认开启批处理了,所以不需要添加这个参数也行)
     * */
    @Test
    public void test2() {
        Connection connection = null;
        PreparedStatement ps = null;
        try {

            long start = System.currentTimeMillis();

            connection = JDBCUtils.getConnection();

            String sql = "insert into goods(name)values (?)";
            ps = connection.prepareStatement(sql);


            for (int i = 0; i < 20000; i++) {
                ps.setObject(1, "name_" + i);

                //1:"攒"sql (缓存一批sql)
                ps.addBatch();
                if (i % 500 == 0) {
                    //2:执行batch
                    ps.executeBatch();
                    //3:清空batch
                    ps.clearBatch();
                }
            }
            long end = System.currentTimeMillis();
            System.out.println("花费的时间为:" + (end - start));//花费的时间为:5336毫秒
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection, ps);
        }
    }

    /*
    * 批量插入方式三:设置不允许自动提交
    *
    * */
    @Test
    public void test3() {
        Connection connection = null;
        PreparedStatement ps = null;
        try {

            long start = System.currentTimeMillis();

            connection = JDBCUtils.getConnection();

            //设置不允许自动提交
            connection.setAutoCommit(false);

            String sql = "insert into goods(name)values (?)";
            ps = connection.prepareStatement(sql);


            for (int i = 0; i < 20000; i++) {
                ps.setObject(1, "name_" + i);

                //1:"攒"sql (缓存一批sql)
                ps.addBatch();
                if (i % 500 == 0) {
                    //2:执行batch
                    ps.executeBatch();
                    //3:清空batch
                    ps.clearBatch();
                }
            }
            long end = System.currentTimeMillis();
            System.out.println("花费的时间为:" + (end - start));//花费的时间为:3451毫秒
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection, ps);
        }
    }
}
