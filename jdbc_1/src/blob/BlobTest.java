package blob;

import bean.Customer;
import org.junit.Test;
import util.JDBCUtils;

import java.io.*;
import java.sql.*;

/*
* 测试使用PreparedStatement存储blob类型的数据
*
* */
public class BlobTest {

    //向数据表customers表中插入blob类型的字段
    @Test
    public void testInsert() {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = JDBCUtils.getConnection();
            String sql = "insert into customers(name, email, birth, photo) values (?,?,?,?)";
            ps = connection.prepareStatement(sql);

            ps.setObject(1,"zzf");
            ps.setObject(2,"zzf@126.com");
            ps.setObject(3,"1990-9-9");

            FileInputStream is = new FileInputStream(new File("/Users/zuozhongfei/Desktop/学习/JAVA/JDBC/JDBC/jdbc_1/src/blob/headIcon.jpg"));
            ps.setBlob(4,is);
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,ps);
        }
    }

    //查询数据表中的blob字段
    @Test
    public void testQuery() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();
            String sql = "select id,name,email,birth,photo from customers where id = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setObject(1,25);

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                //方式一:使用索引
    //            int id = resultSet.getInt(1);
    //            String name = resultSet.getString(2);
    //            String email = resultSet.getString(3);
    //            Date birth = resultSet.getDate(4);

                //方式二:使用列名
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                Date birth = resultSet.getDate("birth");

                Customer customer = new Customer(id, name, email, birth);
                System.out.println(customer);

                //将blob类型的字段下载下来保存到本地,以文件的方式
                Blob photo = resultSet.getBlob("photo");

                InputStream binaryStream = null;
                FileOutputStream fos = null;
                try {
                    binaryStream = photo.getBinaryStream();
                    fos = new FileOutputStream("zzf.jpg");
                    byte[] bytes = new byte[1024];
                    int len;
                    while ((len = binaryStream.read(bytes)) != -1) {
                        fos.write(bytes,0,len);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (binaryStream != null)
                        binaryStream.close();
                    if (fos != null)
                        fos.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,preparedStatement,resultSet);
        }
    }
}
