package exer;

import org.junit.Test;
import util.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

//向表中插入一条数据
public class Exer1 {

    @Test
    public void testInsert(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用户名:");
        String name = scanner.next();

        System.out.println("请输入邮箱:");
        String email = scanner.next();

        System.out.println("请输入生日:");
        String birth = scanner.next();
        scanner.close();
        String sql = "insert into customers(name,email,birth) values(?,?,?)";
        int insertCount = update(sql, name, email, birth);

        if (insertCount > 0) {
            System.out.println("影响了" + insertCount + "行数据");
        }else{
            System.out.println("插入数据失败");
        }
    }

    //通用的增删改操作(对于增删改操作)
    public static int update(String sql, Object... args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            //1:获取数据库连接
            connection = JDBCUtils.getConnection();

            //2:预编译sql语句
            preparedStatement = connection.prepareStatement(sql);

            //3:填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }

            /*
            * 4:执行
            * 如果执行的是查询操作,有返回结果,此方法返回true,
            * 如果是增删改操作,没有返回结果,这个方法返回false
            * */
//            preparedStatement.execute();

            /*
            * 此方法返回操作改变的行数,如果返回0,说明一行都没有影响
            * */
            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5:关闭
            JDBCUtils.closeResouce(connection,preparedStatement);
        }
        return 0;
    }
}
