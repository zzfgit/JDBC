package exer;

import exer.bean.Student;
import org.junit.Test;
import util.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.Scanner;

/*
* 练习2
* */
public class Exer2 {

    //问题1:向examstudent表中添加一条记录
    @Test
    public void testInsert(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("四级/六级:");
        int type = scanner.nextInt();
        System.out.print("身份证号:");
        String IDCard = scanner.next();
        System.out.print("准考证号:");
        String examCard = scanner.next();
        System.out.print("学生姓名:");
        String studentName = scanner.next();
        System.out.print("所在城市:");
        String location = scanner.next();
        System.out.print("考试成绩:");
        int grade = scanner.nextInt();

        String sql = "insert into examstudent(type,IDCard,examCard,studentName,location,grade)values(?,?,?,?,?,?)";
        int insertCount = update(sql, type, IDCard, examCard, studentName, location, grade);
        if (insertCount > 0) {
            System.out.println("添加成功");
        }else {
            System.out.println("添加失败");
        }
    }

    //问题2:根据身份证号,或者准考证号查询学生成绩
    @Test
    public void querywithIDCardOrExamCard(){
        System.out.println("请选择要输入的类型:");
        System.out.println("a.准考证号");
        System.out.println("b.身份证号");

        Scanner scanner = new Scanner(System.in);
        String selection = scanner.next();
        if ("a".equalsIgnoreCase(selection)){
            System.out.println("请输入准考证号:");
            String examCard = scanner.next();

            String sql = "select FlowID flowID,Type type,IDCard ,ExamCard examCard,StudentName studentName,Location location,grade from examstudent where ExamCard = ?";

            Student student = getInstance(Student.class, sql, examCard);
            if (student != null){
                System.out.println(student);
            }else{
                System.out.println("输入的准考证号有误");
            }
        }else if("b".equalsIgnoreCase(selection)){
            System.out.println("请输入身份证号:");
            String IDCard = scanner.next();

            String sql = "select FlowID flowID,Type type,IDCard ,ExamCard examCard,StudentName studentName,Location location,grade from examstudent where IDCard = ?";

            Student student = getInstance(Student.class, sql, IDCard);
            if (student != null){
                System.out.println(student);
            }else{
                System.out.println("输入的身份证号有误");
            }
        }else {
            System.out.println("您的输入有误,请重新进入程序!");
        }
    }

    //问题3:删除一条数据
    @Test
    public void testDeleteByExamCard(){
        System.out.println("请输入学生的考号:");
        Scanner scanner = new Scanner(System.in);
        String examCard = scanner.next();

        String sql = "select FlowID flowID,Type type,IDCard ,ExamCard examCard,StudentName studentName,Location location,grade from examstudent where ExamCard = ?";
        //查询指定准考证号的学生
        Student student = getInstance(Student.class, sql, examCard);
        if (student == null){
            System.out.println("查无此人,请重新输入");
        }else{
            String sql1 = "delete from examstudent where ExamCard = ?";
            int deleteCount = update(sql1, examCard);
            if (deleteCount>0){
                System.out.println("删除成功");
            }else {
                System.out.println("删除失败");
            }
        }
    }

    //问题3:删除一条数据,优化版
    @Test
    public void testDeleteByExamCard1(){
        System.out.println("请输入学生的考号:");
        Scanner scanner = new Scanner(System.in);
        String examCard = scanner.next();

        String sql1 = "delete from examstudent where ExamCard = ?";
        int deleteCount = update(sql1, examCard);
        if (deleteCount>0){
            System.out.println("删除成功");
        }else {
            System.out.println("查无此人");
        }
    }

    //通用的增删改操作(对于增删改操作)
    public static int update(String sql, Object... args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            //1:获取数据库连接
            connection = JDBCUtils.getConnection();

            //2:预编译sql语句
            preparedStatement = connection.prepareStatement(sql);

            //3:填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }

            /*
             * 4:执行
             * 如果执行的是查询操作,有返回结果,此方法返回true,
             * 如果是增删改操作,没有返回结果,这个方法返回false
             * */
//            preparedStatement.execute();

            /*
             * 此方法返回操作改变的行数,如果返回0,说明一行都没有影响
             * */
            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5:关闭
            JDBCUtils.closeResouce(connection,preparedStatement);
        }
        return 0;
    }

    //针对不同表的通用查询操作方法
    public <T> T getInstance(Class<T> clazz,String sql,Object ...args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1,args[i]);
            }

            //执行,获取结果集
            resultSet = preparedStatement.executeQuery();

            //获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //获取列数
            int columnCount = metaData.getColumnCount();
            if (resultSet.next()) {

                T t = clazz.newInstance();

                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过ResultSet
                    Object columnValue = resultSet.getObject(i + 1);
                    //获取列的别名
                    String columnLabel = metaData.getColumnLabel(i + 1);
                    //通过反射,将对象指定名columnName的属性赋值为指定的值columnValue
                    Field field = clazz.getDeclaredField(columnLabel);
                    field.setAccessible(true);
                    field.set(t,columnValue);
                }
                return t;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,preparedStatement,resultSet);
        }

        return null;
    }

}
