package util;

import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/*
* 操作数据库的工具类
* */
public class JDBCUtils {

    /*
    * 获取数据库连接
    * */
    public static Connection getConnection() throws Exception{
        //1.读取配置文件中的信息
        InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream("jdbc.properties");
        Properties pros = new Properties();
        pros.load(is);

        String user = pros.getProperty("user");
        String password = pros.getProperty("password");
        String url = pros.getProperty("url");
        String driverClass = pros.getProperty("driverClass");

        //2:加载驱动
        Class.forName(driverClass);

        //3:获取连接
        Connection connection = DriverManager.getConnection(url, user, password);
        System.out.println(connection);
        return connection;
    }

    /*
    * 关闭连接和statement的资源
    * */
    public static void closeResouce(Connection connection, Statement ps){
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (ps != null)
                ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
    * 关闭资源
    * */
    public static void closeResouce(Connection connection, Statement ps, ResultSet resultSet){
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (ps != null)
                ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null)
                resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
