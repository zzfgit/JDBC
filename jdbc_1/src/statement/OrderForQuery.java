package statement;

import bean.Order;
import org.junit.Test;
import util.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.*;

/*
* 针对Order表的通用的查询操作
*
* */
public class OrderForQuery {

    @Test
    public void testQuery1() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();
            String sql = "SELECT order_id,order_name,order_date FROM `order` WHERE order_id = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setObject(1,1);

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int id = (int) resultSet.getObject(1);
                String name = (String) resultSet.getObject(2);
                Date date = (Date) resultSet.getObject(3);

                Order order = new Order(id, name, date);
                System.out.println(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,preparedStatement,resultSet);
        }
    }

    /*
    * 针对Order表的通用的查询操作
    *
    * 对于表的字段名与类的属性名不一致的情况:
    * 1:必须声明sql时,使用类的属性名来命名字段的别名
    * 2:使用resultSetMetaData时,使用getColumnLabel替代getColumnName来获取列的别名
    *   说明:如果sql语句中没有给列起别名,getColumnLabel获取到的就是列名
    * */
    public Order orderForQuery(String sql,Object ...args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1,args[i]);
            }

            //执行,获取结果集
            resultSet = preparedStatement.executeQuery();

            //获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //获取列数
            int columnCount = metaData.getColumnCount();
            if (resultSet.next()) {
                Order order = new Order();

                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过ResultSet
                    Object columnValue = resultSet.getObject(i + 1);
                    //获取每一个列的列名:ResultSetMetaData
//                    String columnName = metaData.getColumnName(i +1);
                    //获取列的别名
                    String columnLabel = metaData.getColumnLabel(i + 1);
                    //通过反射,将对象指定名columnName的属性赋值为指定的值columnValue
                    Field field = Order.class.getDeclaredField(columnLabel);
                    field.setAccessible(true);
                    field.set(order,columnValue);
                }
                return order;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,preparedStatement,resultSet);
        }

        return null;
    }

    @Test
    public void testOrderForQuery(){
        String sql = "SELECT order_id orderId,order_name orderName,order_date orderDate FROM `order` WHERE order_id = ?";
        Order order = orderForQuery(sql,1);
        System.out.println(order);
    }
}
