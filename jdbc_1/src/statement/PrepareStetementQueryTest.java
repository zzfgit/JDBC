package statement;

import bean.Customer;
import bean.Order;
import org.junit.Test;
import util.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/*
* 通过PreparedStatement实现对于不同表的通用查询操作
* */
public class PrepareStetementQueryTest {

    @Test
    public void testGetInstance(){
        String sql = "SELECT id,name,email,birth FROM customers WHERE id = ?";
        Customer customer = getInstance(Customer.class, sql, 2);
        System.out.println(customer);

        String sql1 = "select order_id orderId,order_name orderName from `order` where order_id = ?";
        Order order = getInstance(Order.class,sql1,2);
        System.out.println(order);


    }

    @Test
    public void testGetForList(){
        String sql1 = "select order_id orderId,order_name orderName from `order` where order_id < ?";
        List list = getForList(Order.class,sql1,5);
//        for (Object o : list) {
//            System.out.println(o);
//        }
        //使用lambda表达式遍历集合,并输出
        list.forEach(System.out::println);

        //查询customers中的所有数据
        String sql2 = "SELECT id,name,email,birth FROM customers";
        List list1 = getForList(Customer.class, sql2);
        list1.forEach(System.out::println);

    }
    //针对不同表的通用查询操作方法
    //此时返回表中的一条数据
    public <T> T getInstance(Class<T> clazz,String sql,Object ...args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1,args[i]);
            }

            //执行,获取结果集
            resultSet = preparedStatement.executeQuery();

            //获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //获取列数
            int columnCount = metaData.getColumnCount();
            if (resultSet.next()) {

                T t = clazz.newInstance();

                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过ResultSet
                    Object columnValue = resultSet.getObject(i + 1);
                    //获取列的别名
                    String columnLabel = metaData.getColumnLabel(i + 1);
                    //通过反射,将对象指定名columnName的属性赋值为指定的值columnValue
                    Field field = clazz.getDeclaredField(columnLabel);
                    field.setAccessible(true);
                    field.set(t,columnValue);
                }
                return t;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,preparedStatement,resultSet);
        }

        return null;
    }

    //针对不同表的查询操作,返回一组数据
    public <T> List<T> getForList(Class<T> clazz,String sql,Object ...args){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i + 1,args[i]);
            }

            //执行,获取结果集
            resultSet = preparedStatement.executeQuery();

            //获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //获取列数
            int columnCount = metaData.getColumnCount();

            //创建集合对象
            ArrayList<T> list = new ArrayList<>();
            while (resultSet.next()) {

                T t = clazz.newInstance();

                //给t对象赋值的过程
                for (int i = 0; i < columnCount; i++) {
                    //获取每一个列的列值:通过ResultSet
                    Object columnValue = resultSet.getObject(i + 1);
                    //获取列的别名
                    String columnLabel = metaData.getColumnLabel(i + 1);
                    //通过反射,将对象指定名columnName的属性赋值为指定的值columnValue
                    Field field = clazz.getDeclaredField(columnLabel);
                    field.setAccessible(true);
                    field.set(t,columnValue);
                }
                list.add(t);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,preparedStatement,resultSet);
        }

        return null;
    }

}
