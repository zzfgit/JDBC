package statement;

import connection.connectionTest;
import org.junit.Test;
import util.JDBCUtils;

import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Properties;

/*
 * 使用PrepareStatement实现对数据表的增删改查操作
 * 增删改:
 * 查:
 * */
public class PrepareStamentUpdate {
    //向customers表中添加一条纪律
    @Test
    public void testInsert1() {
        Connection connection = null;
        PreparedStatement ps = null;

        try {
            //1.读取配置文件中的信息
            InputStream is = connectionTest.class.getClassLoader().getResourceAsStream("jdbc.properties");
            Properties pros = new Properties();
            pros.load(is);

            String user = pros.getProperty("user");
            String password = pros.getProperty("password");
            String url = pros.getProperty("url");
            String driverClass = pros.getProperty("driverClass");

            //2:加载驱动
            Class.forName(driverClass);

            //3:获取连接
            connection = DriverManager.getConnection(url, user, password);
            System.out.println(connection);

            //4:预编译sql语句,返回prepareStatement实例
            String sql = "INSERT INTO customers (name,email,birth) VALUES (?,?,?)";
            ps = connection.prepareStatement(sql);

            //5:填充占位符
            ps.setString(1, "哪吒");
            ps.setString(2, "nezha@gmail.com");
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
            java.util.Date date = sdf.parse("1000-1-1");
            ps.setDate(3, new Date(date.getTime()));

            //6:执行sql
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //7:资源的关闭
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (ps != null)
                    ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //修改一条纪律
    @Test
    public void testUpdate1(){
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            //1:获取数据库的连接
            connection = JDBCUtils.getConnection();
            //2:预编译sql语句,返回PrepareStatement实例
            String sql = "UPDATE customers SET name= ? WHERE id = ?";
            ps = connection.prepareStatement(sql);

            //3:填充占位符
            ps.setObject(1, "卡特琳娜");
            ps.setObject(2, 18);

            //4:执行操作
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5:资源关闭
            JDBCUtils.closeResouce(connection,ps);
        }

    }

    //通用的增删改操作(对于增删改操作)
    public void update(String sql,Object ...args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            //1:获取数据库连接
            connection = JDBCUtils.getConnection();

            //2:预编译sql语句
            preparedStatement = connection.prepareStatement(sql);

            //3:填充占位符
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }

            //4:执行
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //5:关闭
            JDBCUtils.closeResouce(connection,preparedStatement);
        }
    }

    @Test
    public void testUpdate(){
//        String sql = "delete from customers where id = ?";
//        update(sql,1);

        String sql = "update `order` set order_name = ? where order_id = ?";
        update(sql,"DD","2");
    }
}
