package statement;

import bean.Customer;
import org.junit.Test;
import util.JDBCUtils;

import java.lang.reflect.Field;
import java.sql.*;

/*
 * 针对于Customer表的查询操作
 * */
public class CustomerForQuery {

    @Test
    public void testQuery1(){

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();
            String sql = "SELECT id,name,email,birth FROM customers WHERE id = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setObject(1,3);
            //执行,并返回结果集
            resultSet = preparedStatement.executeQuery();
            //处理结果集
            if (resultSet.next()) {//判断结果集下一条是否有数据,如果有数据,返回true,指针下移,如果返回false,指针不下移
                //获取当前这条数据的各个字段值
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                String email = resultSet.getString(3);
                Date birth = resultSet.getDate(4);

                //方式一:直接显示
//            System.out.println("id = " + id +"name = " + name + "email = " + email + "birth = " + birth);

                //方式二:存入数组
//            Object[] data = new Object[]{id,name,email,birth};

                //方式三:存入对象
                Customer customer = new Customer(id, name, email, birth);
                System.out.println(customer);//bean{id=3, name='林志玲', email='linzl@gmail.com', birth=1984-06-12}

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //关闭资源
            JDBCUtils.closeResouce(connection, preparedStatement, resultSet);
        }
    }

    /*
    * 针对Customers表的通用的查询操作
    * */
    public Customer queryForCustomers(String sql,Object ...args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = JDBCUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < args.length; i++) {
                preparedStatement.setObject(i+1,args[i]);
            }

            resultSet = preparedStatement.executeQuery();

            //获取结果集的元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            //通过ResultSetMetaData获取元数据的列数
            int columnCount = metaData.getColumnCount();

            if (resultSet.next()) {
                Customer customer = new Customer();

                //处理结果集一行数据中的每一个列
                for (int i = 0; i < columnCount; i++) {

                    //获取这一列的值
                    Object value = resultSet.getObject(i + 1);

                    //获取每个列的列名
                    String columnName = metaData.getColumnName(i + 1);

                    //给customer指定的columnName属性赋值为value,通过反射
                    Field field = Customer.class.getDeclaredField(columnName);
                    field.setAccessible(true);
                    field.set(customer,value);
                }
                return customer;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtils.closeResouce(connection,preparedStatement,resultSet);
        }

        return null;
    }

    @Test
    public void testQueryForCustomers(){
        String sql = "select id,name,birth,email from customers where id = ?";
        Customer customer = queryForCustomers(sql, 13);
        System.out.println(customer);
    }
    //TODO:数据库存储中文都是 ? ,待修改
}
